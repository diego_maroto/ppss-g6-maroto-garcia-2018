/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    private CalendarioStub calStub = new CalendarioStub();
    private GestorLlamadasStub gestorStub = new GestorLlamadasStub();
    private int minutos;
    private int hora;

    @Test
    public void testCalculaConsumoC1() {
        System.out.println("calculaConsumoC1");
        minutos = 10;
        hora = 15;
        double expResult = 208;
        calStub.setHoraActual(hora);
        gestorStub.setCalendario(calStub);
        double result = gestorStub.calculaConsumo(minutos);
        assertEquals(expResult, result, 0.0);
    }
    @Test
    public void testCalculaConsumoC2() {
        System.out.println("calculaConsumoC2");
        minutos = 10;
        hora = 22;
        double expResult = 105.0;
        calStub.setHoraActual(hora);
        gestorStub.setCalendario(calStub);
        double result = gestorStub.calculaConsumo(minutos);
        assertEquals(expResult, result, 0.0);
    }
}
