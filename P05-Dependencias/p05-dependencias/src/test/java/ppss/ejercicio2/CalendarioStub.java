/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

/**
 *
 * @author ppss
 */
public class CalendarioStub extends Calendario{
    private int horaActual;
    public void setHoraActual(int hora){
        horaActual = hora;
    }
    @Override
    public int getHoraActual(){
        return horaActual;
    }
}
