/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    private GestorLlamadasTestable testable = new GestorLlamadasTestable();
    private int resultadoEsperado;
    private int minutos;
    public GestorLlamadasTest() {
    }

    @Test
    public void testCalculaConsumoC1() {
        resultadoEsperado = 208;
        testable.setHoraActual(15);
        minutos = 10;
        double resultadoReal = testable.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal, 0.0);
    }
    @Test
    public void testCalculaConsumoC2() {
        resultadoEsperado = 105;
        testable.setHoraActual(22);
        minutos = 10;
        double resultadoReal = testable.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal, 0.0);
    }
}
