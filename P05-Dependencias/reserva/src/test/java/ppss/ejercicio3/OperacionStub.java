/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.ArrayList;
import java.util.List;
import ppss.ejercicio3.excepciones.*;

/**
 *
 * @author ppss
 */
public class OperacionStub extends Operacion{
    private boolean accesoDB;
    private List<String> listaSocios;
    private List<String> listaIsbns;
    
    public OperacionStub(){
        listaSocios = new ArrayList<String>();
        listaIsbns = new ArrayList<String>();
        listaSocios.add("Luis");
        listaIsbns.add("11111");
        listaIsbns.add("22222");
        listaIsbns.add("33333");
    }

    @Override
    public void operacionReserva(String socio, String isbn)
            throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
        if(!accesoDB)
            throw new JDBCException();
        if(!listaSocios.contains(socio))
            throw new SocioInvalidoException();
        if(!listaIsbns.contains(isbn))
            throw new IsbnInvalidoException();
    }
    public void setAccesoDB(boolean accesoDB){
        this.accesoDB = accesoDB;
    }
    public void setListaSocio(List<String> listaSocios){
        this.listaSocios = listaSocios;
    }
    public void setListaIsbns(List<String> listaIsbns){
        this.listaIsbns = listaIsbns;
    }
}
