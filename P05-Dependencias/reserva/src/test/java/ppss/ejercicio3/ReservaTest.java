/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio3.excepciones.ReservaException;

/**
 *
 * @author ppss
 */
public class ReservaTest {
    private String login, password, socio;
    private boolean db;
    private final String[] sociosDB;
    private final String[] isbnsDB;
    private String[] isbns;
    private final ReservaStub stub;
    private boolean permisos;
    private final OperacionStub operacion;
    private ReservaException expected;
    
    public ReservaTest(){
        sociosDB = new String[] {"Luis"};
        isbnsDB = new String[] {"111111","22222"};
        stub = new ReservaStub();
        operacion = new OperacionStub();
        operacion.setListaSocio(new ArrayList(Arrays.asList(sociosDB)));
        operacion.setListaIsbns(new ArrayList(Arrays.asList(isbnsDB)));
    }
    
    @Test
    public void testRealizaReservaC1() {
        expected = new ReservaException("ERROR de permisos; ");
        login = "xxxx";
        password = "xxxx";
        socio = "Luis";
        isbns = new String[] {"111111"};
        db = true;
        permisos = false;
        operacion.setAccesoDB(db);
        stub.setPermisos(permisos);
        FactoryOperacion.setOperacion(operacion);
        try{
            stub.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected.toString(), e.toString());
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }
    @Test
    public void testRealizaReservaC2() {
        expected = new ReservaException("");
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[] {"111111","22222"};
        db = true;
        permisos = true;
        operacion.setAccesoDB(db);
        stub.setPermisos(permisos);
        FactoryOperacion.setOperacion(operacion);
        try{
            stub.realizaReserva(login, password, socio, isbns);
        }catch(ReservaException e){
            fail();
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }
    @Test
    public void testRealizaReservaC3(){
        expected = new ReservaException("ISBN invalido:33333; ");
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[] {"33333"};
        db = true;
        permisos = true;
        operacion.setAccesoDB(db);
        stub.setPermisos(permisos);
        FactoryOperacion.setOperacion(operacion);
        try{
            stub.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected.toString(), e.toString());
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }
    @Test
    public void testRealizaReservaC4(){
        expected = new ReservaException("SOCIO invalido; ");
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[] {"111111"};
        db = true;
        permisos = true;
        operacion.setAccesoDB(db);
        stub.setPermisos(permisos);
        FactoryOperacion.setOperacion(operacion);
        try{
            stub.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected.toString(), e.toString());
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }
    @Test
    public void testRealizaReservaC5(){
        expected = new ReservaException("CONEXION invalida; ");
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[] {"111111"};
        db = false;
        permisos = true;
        operacion.setAccesoDB(db);
        stub.setPermisos(permisos);
        FactoryOperacion.setOperacion(operacion);
        try{
            stub.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected.toString(), e.toString());
        } catch (Exception ex) {
            fail(ex.toString());
        }
    }
}