/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

/**
 *
 * @author ppss
 */
public class FactoryOperacion{
    public static IOperacionBO operacion;
    
    public static IOperacionBO getOperacion(){
        if(operacion == null){
            return new Operacion();
        }else{
            return operacion;
        }
    }
    public static void setOperacion(IOperacionBO o){
        operacion = o;
    }
}
