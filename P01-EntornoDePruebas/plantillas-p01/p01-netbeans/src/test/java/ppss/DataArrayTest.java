/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;
import java.util.Arrays;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class DataArrayTest {
    private DataArray dataarray;
    
    @Before
    public void init(){
        dataarray = new DataArray(new int[11],0);
    }
    
    @Test
    public void testAddElemToEmptyC1() {
        int elem = 23;
        dataarray.add(elem);
        int esperado = 1;
        int real = dataarray.size();
        assertEquals(esperado, real);
        int[] arrayEsperado = new int[]{23,0,0,0,0,0,0,0,0,0,0};
        assertEquals(Arrays.toString(arrayEsperado),Arrays.toString(dataarray.getColeccion()));
    }
    
    @Test
    public void testAddElemToOneElementC2() {
        int elem = 23;
        dataarray.add(elem);
        dataarray.add(elem+2);
        int esperado = 2;
        int real = dataarray.size();
        assertEquals(esperado, real);
        int[] arrayEsperado = new int[]{23,25,0,0,0,0,0,0,0,0,0};
        assertEquals(Arrays.toString(arrayEsperado),Arrays.toString(dataarray.getColeccion()));
    }
    
    @Test
    public void testAddElemToFullC3() {
        int elem = 23;
        int incremento = 2;
        for(int i=0;i<15;i++){
            dataarray.add(elem);
            elem+=incremento;
        }
        int esperado = 11;
        int real = dataarray.size();
        assertEquals(esperado, real);
        int[] arrayEsperado = new int[]{23,25,27,29,31,33,35,37,39,41,43};
        assertEquals(Arrays.toString(arrayEsperado),Arrays.toString(dataarray.getColeccion()));
    }
    
    private void initArrayForDelete(){
        dataarray = new DataArray(new int[]{1,7,3,0,0,0,0,0,0,0,0},3);
    }
    
    @Test
    public void testDeleteElementC4(){
        initArrayForDelete();
        dataarray.delete(7);
        int esperado = 2;
        int real = dataarray.size();
        assertEquals(esperado, real);
        int[] colectionArray = dataarray.getColeccion();
        assertEquals(1,colectionArray[0]);
        assertEquals(3,colectionArray[1]);
    }
    
    @Test
    public void testDeleteElementC5(){
        initArrayForDelete();
        dataarray.delete(4);
        int esperado = 3;
        int real = dataarray.size();
        assertEquals(esperado, real);
        int[] colectionArray = dataarray.getColeccion();
        assertEquals(1,colectionArray[0]);
        assertEquals(7,colectionArray[1]);
        assertEquals(3,colectionArray[2]);
    }
}
