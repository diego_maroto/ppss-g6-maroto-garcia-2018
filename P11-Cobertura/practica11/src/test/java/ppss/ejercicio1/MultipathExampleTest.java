/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class MultipathExampleTest {
    MultipathExample testable;
    int a,b,c,real,expected;
    public MultipathExampleTest() {
    }
    
    @Before
    public void setUp() {
        testable = new MultipathExample();
    }
    @Test
    public void testExampleC3(){
        boolean a = true;
        boolean b = false;
        boolean c = false;
        real = testable.example(a, b);
        assertEquals(0,real);
    }
    @Test
    public void testExampleC4(){
        boolean a = false;
        boolean b = false;
        real = testable.example(a, b);
        assertEquals(0,real);
    }
}
