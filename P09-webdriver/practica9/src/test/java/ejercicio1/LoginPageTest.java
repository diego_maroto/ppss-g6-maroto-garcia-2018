/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class LoginPageTest {
    private LoginPage loginPage;
    private WebDriver webDriver;
    
    @Before
    public void setUp() {
        webDriver = new FirefoxDriver();
        
        loginPage = PageFactory.initElements(webDriver, LoginPage.class);
    }

    /**
     * Test of login method, of class LoginPage.
     */
    @Test
    public void test_Login_Incorrect() {
        
        String loginPageTitle = loginPage.getPageTitle();
        assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        loginPage.login("usermal","passmal");
        Alert alert = webDriver.switchTo().alert();
        String mensaje = alert.getText();
        assertEquals("User or Password is not valid", mensaje);
        alert.accept();
        webDriver.close();
    }
}
