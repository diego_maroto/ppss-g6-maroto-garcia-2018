/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.pageFactory;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author ppss
 */
public class NewCustomerPageTest {
    String userOK = "mngr128897";
    String passOK = "aqAzatu";
    
    WebDriver driver;
    LoginPage loginPage;
    ManagerPage managerPage;
    NewCustomerPage newCustomerPage;
    @Before
    public void setUp() {
        driver = new FirefoxDriver();
        loginPage = PageFactory.initElements(driver, LoginPage.class);
    }

    /**
     * Test of addNewCustomer method, of class NewCustomerPage.
     */
    @Test
    public void testAddNewCustomer() {
        managerPage = loginPage.login(userOK,passOK);
        newCustomerPage = managerPage.newCustomer();
        newCustomerPage.addNewCustomer("dmg", Gender.m, 
                "13/03/1986", "Calle x", "Alicante", "123456", 
                "999999999", "dmg@alu.ua.es", "123456");
        assertTrue(driver.getPageSource().contains("Customer Registered Successfully!!!"));
        driver.close();
    }
    
}
