/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2.pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author ppss
 */
public class NewCustomerPage {

    WebDriver driver;
    @FindBy(xpath = "//table//tr[@class='heading3']")
    WebElement pageTitle;
    @FindBy(name = "name")
    WebElement customerName;
    @FindBy(css = "input[value='m']")
    WebElement genderRadioM;
    @FindBy(css = "input[value='f']")
    WebElement genderRadioF;
    @FindBy(name = "dob")
    WebElement fecha;
    @FindBy(name = "addr")
    WebElement address;
    @FindBy(name = "city")
    WebElement city;
    @FindBy(name = "state")
    WebElement state;
    @FindBy(name = "pinno")
    WebElement pin;
    @FindBy(name = "telephoneno")
    WebElement telefono;
    @FindBy(name = "emailid")
    WebElement email;
    @FindBy(name = "password")
    WebElement password;
    @FindBy(css = "input[value='Submit']")
    WebElement submit;

    public NewCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPageTitle() {
        return pageTitle.getText();
    }

    public void addNewCustomer(String customerName, Gender gender, String fechaNacimiento,
            String direccion, String ciudad, String pin, String movil,
            String email, String password) {
        this.customerName.sendKeys(customerName);
        if(gender == Gender.f){
            this.genderRadioF.click();
        }else{
            this.genderRadioM.click();
        }
        this.fecha.click();
        String[] fechaFormat = fechaNacimiento.split("/");
        this.fecha.sendKeys(fechaFormat[2]+"-"+fechaFormat[1]+"-"+fechaFormat[0]);
        this.address.click();
        this.address.sendKeys(direccion);
        this.city.sendKeys(ciudad);
        this.state.sendKeys(ciudad);
        this.pin.sendKeys(pin);
        this.telefono.sendKeys(movil);
        this.email.sendKeys(email);
        this.password.sendKeys(password);
        this.submit.click();
    }
}
