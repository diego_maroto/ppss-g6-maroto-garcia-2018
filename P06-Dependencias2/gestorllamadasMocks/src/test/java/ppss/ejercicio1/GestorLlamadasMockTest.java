/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author ppss
 */
public class GestorLlamadasMockTest {

    private Calendario c;
    private GestorLlamadas testObject;
    private int minutos;
    private int hora;
    private double resultadoEsperado;
    private double resultadoReal;

    @Before
    public void setUp() {
        testObject = EasyMock.partialMockBuilder(GestorLlamadas.class)
                .addMockedMethod("getCalendario")
                .createMock();
        c = EasyMock.createMock(Calendario.class);
    }

    @Test
    public void testCalculaConsumoC1() {
        System.out.println("calculaConsumoC1");
        minutos = 22;
        hora = 10;
        resultadoEsperado = 457.6;
        EasyMock.expect(c.getHoraActual()).andReturn(hora);
        EasyMock.replay(c);
        EasyMock.expect(testObject.getCalendario()).andReturn(c);
        EasyMock.replay(testObject);
        resultadoReal = testObject.calculaConsumo(minutos);
        Assert.assertEquals(resultadoEsperado, resultadoReal, 0.0);
        EasyMock.verify(testObject,c);
    }
    @Test
    public void testCalculaConsumoC2() {
        System.out.println("calculaConsumoC2");
        minutos = 13;
        hora = 21;
        resultadoEsperado = 136.5;
        EasyMock.expect(c.getHoraActual()).andReturn(hora);
        EasyMock.replay(c);
        EasyMock.expect(testObject.getCalendario()).andReturn(c);
        EasyMock.replay(testObject);
        resultadoReal = testObject.calculaConsumo(minutos);
        Assert.assertEquals(resultadoEsperado, resultadoReal, 0.0);
        EasyMock.verify(testObject,c);
    }
}
