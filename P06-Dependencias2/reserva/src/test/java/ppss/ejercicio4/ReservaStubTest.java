/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import org.easymock.EasyMock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */
public class ReservaStubTest {
    private String login;
    private String password;
    private String ident_socio;
    private Usuario tipo_socio;
    private boolean permisos;
    private String[] isbns;
    
    private String[] db_isbns;
    private String[] db_socios;
    
    private Reserva testObject;
    private FactoriaBOs factoryMock;
    private IOperacionBO ioperacionMock;
    
    private ReservaException resultadoEsperado;
    private ReservaException resultadoReal;
    
    public ReservaStubTest(){
        db_isbns = new String[] {"22222","33333"};
        db_socios = new String[] {"Pepe"};
    }
    
    @Before
    public void setUp() {
        testObject = EasyMock.partialMockBuilder(Reserva.class)
                .addMockedMethod("getFactoriaBOs")
                .addMockedMethod("compruebaPermisos")
                .createNiceMock();
        factoryMock = EasyMock.createNiceMock(FactoriaBOs.class);
        ioperacionMock = EasyMock.createNiceMock(IOperacionBO.class);
        permisos = true;
        tipo_socio = Usuario.BIBLIOTECARIO;
    }

    @Test
    public void testRealizaReservaC1() throws Exception {
        System.out.println("testRealizaReservaC1");
        permisos = false;
        login = "xxxx";
        password = "xxxx";
        ident_socio = "Pepe";
        isbns = new String[]{"22222"};
        resultadoEsperado = new ReservaException("ERROR de permisos; ");
        
        EasyMock.expect(testObject.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))
                .andStubReturn(permisos);
        EasyMock.replay(testObject);
        try{
            testObject.realizaReserva(login, password, ident_socio, isbns);
            fail();
        }catch(ReservaException e){
            resultadoReal = e;
            assertEquals(resultadoEsperado.getMessage(), resultadoReal.getMessage());
        }
    }
    @Test
    public void testRealizaReservaC2() throws Exception {
        System.out.println("testRealizaReservaC2");
        login = "ppss";
        password = "ppss";
        ident_socio = "Pepe";
        isbns = new String[]{"22222","33333"};
        resultadoEsperado = null;
        
        ioperacionMock.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
        EasyMock.expectLastCall().anyTimes();
        EasyMock.replay(ioperacionMock);
        
        EasyMock.expect(factoryMock.getOperacionBO()).andStubReturn(ioperacionMock);
        EasyMock.replay(factoryMock);
        
        EasyMock.expect(testObject.getFactoriaBOs()).andStubReturn(factoryMock);
        EasyMock.expect(testObject.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))
                .andStubReturn(permisos);
        EasyMock.replay(testObject);
        
        testObject.realizaReserva(login, password, ident_socio, isbns);
    }
    @Test
    public void testRealizaReservaC3() throws Exception {
        System.out.println("testRealizaReservaC3");
        login = "ppss";
        password = "ppss";
        ident_socio = "Pepe";
        isbns = new String[]{"11111"};
        resultadoEsperado = new ReservaException("ISBN invalido:11111; ");
        
        ioperacionMock.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
        EasyMock.expectLastCall().andStubThrow(new IsbnInvalidoException());
        EasyMock.replay(ioperacionMock);
        
        EasyMock.expect(factoryMock.getOperacionBO()).andStubReturn(ioperacionMock);
        EasyMock.replay(factoryMock);
        
        EasyMock.expect(testObject.getFactoriaBOs()).andStubReturn(factoryMock);
        EasyMock.expect(testObject.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))
                .andStubReturn(permisos);
        EasyMock.replay(testObject);
        try{
            testObject.realizaReserva(login, password, ident_socio, isbns);
            fail();
        }catch(ReservaException e){
            resultadoReal = e;
            assertEquals(resultadoEsperado.getMessage(), resultadoReal.getMessage());
        }
        EasyMock.verify(testObject,ioperacionMock,factoryMock);
    }
    @Test
    public void testRealizaReservaC4() throws Exception {
        System.out.println("testRealizaReservaC4");
        login = "ppss";
        password = "ppss";
        ident_socio = "Luis";
        isbns = new String[]{"22222"};
        resultadoEsperado = new ReservaException("SOCIO invalido; ");
        
        ioperacionMock.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
        EasyMock.expectLastCall().andStubThrow(new SocioInvalidoException());
        EasyMock.replay(ioperacionMock);
        
        EasyMock.expect(factoryMock.getOperacionBO()).andStubReturn(ioperacionMock);
        EasyMock.replay(factoryMock);
        
        EasyMock.expect(testObject.getFactoriaBOs()).andStubReturn(factoryMock);
        EasyMock.expect(testObject.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))
                .andStubReturn(permisos);
        EasyMock.replay(testObject);
        try{
            testObject.realizaReserva(login, password, ident_socio, isbns);
            fail();
        }catch(ReservaException e){
            resultadoReal = e;
            assertEquals(resultadoEsperado.getMessage(), resultadoReal.getMessage());
        }
    }
    @Test
    public void testRealizaReservaC5() throws Exception {
        System.out.println("testRealizaReservaC5");
        login = "xxxx";
        password = "xxxx";
        ident_socio = "Pepe";
        isbns = new String[]{"22222"};
        resultadoEsperado = new ReservaException("CONEXION invalida; ");
        
        ioperacionMock.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
        EasyMock.expectLastCall().andStubThrow(new JDBCException());
        EasyMock.replay(ioperacionMock);
        
        EasyMock.expect(factoryMock.getOperacionBO()).andStubReturn(ioperacionMock);
        EasyMock.replay(factoryMock);
        
        EasyMock.expect(testObject.getFactoriaBOs()).andStubReturn(factoryMock);
        EasyMock.expect(testObject.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject())).andStubReturn(permisos);
        EasyMock.replay(testObject);
        try{
            testObject.realizaReserva(login, password, ident_socio, isbns);
            fail();
        }catch(ReservaException e){
            resultadoReal = e;
            assertEquals(resultadoEsperado.getMessage(), resultadoReal.getMessage());
        }
    }
}
