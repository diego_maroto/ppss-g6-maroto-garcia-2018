/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class WebClientMockTest {
    private WebClient testObject;
    private HttpURLConnection connection;
    private boolean errorConn;
    private URL url;
    private String resultadoEsperado;
    private String resultadoReal;
    
    @Before
    public void setUp() throws MalformedURLException {
        testObject = EasyMock.partialMockBuilder(WebClient.class)
            .addMockedMethod("createHttpURLConnection")
            .createMock();
        connection = EasyMock.createMock(HttpURLConnection.class);
        url = new URL("http://www.google.es");
    }
    @Test
    public void testGetContentC1() throws IOException {
        System.out.println("getContentC1");
        errorConn = false;
        resultadoEsperado = "Se puede abrir la conexión";
        
        EasyMock.expect(connection.getInputStream())
                .andReturn(new ByteArrayInputStream(resultadoEsperado.getBytes()));
        EasyMock.replay(connection);
        
        EasyMock.expect(testObject.createHttpURLConnection(url)).andReturn(connection);
        EasyMock.replay(testObject);
        
        resultadoReal = testObject.getContent(url);
        
        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(testObject,connection);
    }
    @Test
    public void testGetContentC2() throws IOException {
        System.out.println("getContentC1");
        errorConn = true;
        resultadoEsperado = null;
        
        EasyMock.expect(connection.getInputStream())
                .andThrow(new IOException());
        EasyMock.replay(connection);
        
        EasyMock.expect(testObject.createHttpURLConnection(url)).andReturn(connection);
        EasyMock.replay(testObject);
        
        resultadoReal = testObject.getContent(url);
        
        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(testObject,connection);
    }
}
