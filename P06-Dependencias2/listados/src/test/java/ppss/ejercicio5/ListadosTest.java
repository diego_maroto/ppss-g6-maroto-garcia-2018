/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio5;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class ListadosTest {
    private Connection con;
    private Statement stm;
    private ResultSet rs;
    private Listados testObject;
    private String tableName;
    private String resultadoEsperado;
    private String resultadoReal;
    public ListadosTest() {
    }
    
    @Before
    public void setUp() {
        con = EasyMock.createMock(Connection.class);
        stm = EasyMock.createMock(Statement.class);
        rs = EasyMock.createMock(ResultSet.class);
        tableName = "alumnos";
        testObject = new Listados();
    }

    @Test
    public void testPorApellidosC1() throws Exception {
        resultadoEsperado = "Garcia, Planelles, Jorge\nPérez, Verdú, Carmen\n";
        
        EasyMock.expect(con.createStatement()).andReturn(stm);
        EasyMock.expect(stm.executeQuery("SELECT apellido1, apellido2, nombre FROM " + tableName))
            .andReturn(rs);
        EasyMock.expect(rs.next()).andReturn(true);
        EasyMock.expect(rs.getString("apellido1")).andReturn("Garcia");
        EasyMock.expect(rs.getString("apellido2")).andReturn("Planelles");
        EasyMock.expect(rs.getString("nombre")).andReturn("Jorge");
        EasyMock.expect(rs.next()).andReturn(true);
        EasyMock.expect(rs.getString("apellido1")).andReturn("Pérez");
        EasyMock.expect(rs.getString("apellido2")).andReturn("Verdú");
        EasyMock.expect(rs.getString("nombre")).andReturn("Carmen");
        EasyMock.expect(rs.next()).andReturn(false);
        EasyMock.replay(con,stm,rs);
        resultadoReal = testObject.porApellidos(con, tableName);
        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(con,stm,rs);
    }
    @Test
    public void testPorApellidosC2() throws Exception {
        resultadoEsperado = "";
        
        EasyMock.expect(con.createStatement()).andReturn(stm);
        EasyMock.expect(stm.executeQuery("SELECT apellido1, apellido2, nombre FROM " + tableName))
            .andReturn(rs);
        EasyMock.expect(rs.next()).andThrow(new SQLException());
        EasyMock.replay(con,stm,rs);
        try{
            resultadoReal = testObject.porApellidos(con, tableName);
            fail();
        }catch(SQLException e){
            System.out.println("Lanza: "+e.getMessage());
        }
        EasyMock.verify(con,stm,rs);
    }
}
