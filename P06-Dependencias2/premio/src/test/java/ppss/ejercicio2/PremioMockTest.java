/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio2.exceptions.ClienteWebServiceException;

/**
 *
 * @author ppss
 */
public class PremioMockTest {

    private float numRandom;
    private ClienteWebService cliente;
    private String clientePremio;
    private Premio testObject;
    private String resultadoEsperado;
    private String resultadoReal;

    @Before
    public void setUp() {
        cliente = EasyMock.createMock(ClienteWebService.class);
        testObject = EasyMock.partialMockBuilder(Premio.class)
                .addMockedMethod("generaNumero")
                .createMock();
    }

    @Test
    public void testCompruebaPremioC1() throws ClienteWebServiceException {
        System.out.println("compruebaPremioC1");
        numRandom = 0.07f;
        clientePremio = "entrada final Champions";
        resultadoEsperado = "Premiado con " + clientePremio;
        EasyMock.expect(cliente.obtenerPremio()).andReturn(clientePremio);
        EasyMock.replay(cliente);
        testObject.cliente = cliente;
        EasyMock.expect(testObject.generaNumero()).andReturn(numRandom);
        EasyMock.replay(testObject);

        resultadoReal = testObject.compruebaPremio();

        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(cliente,testObject);
    }
    @Test
    public void testCompruebaPremioC2() throws ClienteWebServiceException {
        System.out.println("compruebaPremioC2");
        numRandom = 0.03f;
        clientePremio = "";
        resultadoEsperado = "No se ha podido obtener el premio";
        
        EasyMock.expect(cliente.obtenerPremio()).andThrow(new ClienteWebServiceException());

        EasyMock.replay(cliente);
        testObject.cliente = cliente;
        EasyMock.expect(testObject.generaNumero()).andReturn(numRandom);
        EasyMock.replay(testObject);

        resultadoReal = testObject.compruebaPremio();

        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(cliente,testObject);
    }
}
