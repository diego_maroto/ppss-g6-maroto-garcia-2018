/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

/**
 *
 * @author ppss
 */
public class Tramo {
    private int origen;
    private int longitud;
    public Tramo(){longitud=0;origen=0;}
    public Tramo(int origen, int longitud){
        this.longitud = longitud;
        this.origen = origen;
    }
    public void setLongitud(int longitud){this.longitud = longitud;}
    public int getLongitud(){return longitud;}
    public void setOrigen(int origen){this.origen = origen;}
    public int getOrigen(){return origen;}

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + this.longitud;
        hash = 23 * hash + this.origen;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tramo other = (Tramo) obj;
        if (this.longitud != other.longitud) {
            return false;
        }
        if (this.origen != other.origen) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "Tramo{" + "origen=" + origen + ", longitud=" + longitud + '}';
    }
}
