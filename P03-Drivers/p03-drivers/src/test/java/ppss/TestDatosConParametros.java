/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.categorias.ConParametros;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class TestDatosConParametros {
    @Parameterized.Parameters(name="Caso C{index}: buscarTramoLlanoMasLargo({0}) = {1}")
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
            {new ArrayList<Integer>(Arrays.asList(3)),new Tramo(0,0)}, //C1A
            {new ArrayList<Integer>(Arrays.asList(100,100,100,100)),new Tramo(0,3)}, //C2A
            {new ArrayList<Integer>(Arrays.asList(120,140,180,180,180)),new Tramo(2,2)}, //C3A
            {new ArrayList<Integer>(Arrays.asList(-1)),new Tramo(0,0)}, //C1B
            {new ArrayList<Integer>(Arrays.asList(-1,-1,-1,-1)),new Tramo(0,3)},  //C2B
            {new ArrayList<Integer>(Arrays.asList(120,140,-10,-10,-10)),new Tramo(2,2)}  //C3B
        });
    }
    private ArrayList<Integer> listaLecturas;
    private Datos datos = new Datos();
    private Tramo resultadoEsperado;
    public TestDatosConParametros(ArrayList<Integer> listaLecturas,Tramo esperado) {
        this.listaLecturas = listaLecturas;
        resultadoEsperado = esperado;
    }
    
    @Test
    @Category(ConParametros.class)
    public void testBuscarTramoLlanoMasLargo(){
        assertEquals(resultadoEsperado, datos.buscarTramoLlanoMasLargo(listaLecturas));
    }
}
