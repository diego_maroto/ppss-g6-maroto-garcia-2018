/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import ppss.categorias.SinParametros;
import ppss.categorias.TestLlanosTablaA;
import ppss.categorias.TestLlanosTablaB;

/**
 *
 * @author ppss
 */
public class TestDatosSinParametros {
    int origen,longitud;
    Tramo resultadoEsperado;
    Datos datos = new Datos();
    ArrayList<Integer> listaLecturas;
    
    public TestDatosSinParametros() {
    }

    /**
     * Test of buscarTramoLlanoMasLargo method, of class Datos.
     */
    @Test
    @Category({SinParametros.class,TestLlanosTablaA.class})
    public void TestC1A() {
        listaLecturas = new ArrayList(Arrays.asList(3));
        origen = 0; longitud = 0;
        resultadoEsperado = new Tramo(origen, longitud);
        Tramo resultadoReal = datos.buscarTramoLlanoMasLargo(listaLecturas);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    @Category({SinParametros.class,TestLlanosTablaA.class})
    public void TestC2A() {
        listaLecturas = new ArrayList(Arrays.asList(100,100,100,100));
        origen = 0; longitud = 3;
        resultadoEsperado = new Tramo(origen, longitud);
        Tramo resultadoReal = datos.buscarTramoLlanoMasLargo(listaLecturas);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    @Category({SinParametros.class,TestLlanosTablaA.class})
    public void TestC3A() {
        listaLecturas = new ArrayList(Arrays.asList(120,140,180,180,180));
        origen = 2; longitud = 2;
        resultadoEsperado = new Tramo(origen, longitud);
        Tramo resultadoReal = datos.buscarTramoLlanoMasLargo(listaLecturas);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    @Category(TestLlanosTablaB.class)
    public void TestC1B() {
        listaLecturas = new ArrayList(Arrays.asList(-1));
        origen = 0; longitud = 0;
        resultadoEsperado = new Tramo(origen, longitud);
        Tramo resultadoReal = datos.buscarTramoLlanoMasLargo(listaLecturas);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    @Category(TestLlanosTablaB.class)
    public void TestC2B() {
        listaLecturas = new ArrayList(Arrays.asList(-1,-1,-1,-1));
        origen = 0; longitud = 3;
        resultadoEsperado = new Tramo(origen, longitud);
        Tramo resultadoReal = datos.buscarTramoLlanoMasLargo(listaLecturas);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    @Category(TestLlanosTablaB.class)
    public void TestC3B() {
        listaLecturas = new ArrayList(Arrays.asList(120,140,-10,-10,-10));
        origen = 2; longitud = 2;
        resultadoEsperado = new Tramo(origen, longitud);
        Tramo resultadoReal = datos.buscarTramoLlanoMasLargo(listaLecturas);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
}
