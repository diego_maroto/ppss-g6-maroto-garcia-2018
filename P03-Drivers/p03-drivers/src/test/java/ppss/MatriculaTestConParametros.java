/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class MatriculaTestConParametros {
    @Parameterized.Parameters(name="Caso C{index}: calculaMatricula({0},{1},{2}) = {3}")
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
            {19,false,true,2000.00f}, //C1
            {68,false,true,250.00f}, //C2
            {19,true,true,250.00f}, //C3
            {19,false,false,500.00f}, //C4
            {61,false,false,400.00f}  //C5
        });
    }
    private float esperado;
    private int edad;
    private boolean familiaNumerosa;
    private boolean repetidor;
    private Matricula m = new Matricula();
    public MatriculaTestConParametros(int edad,boolean familiaNumerosa,boolean repetidor, float esperado){
        this.edad = edad;
        this.familiaNumerosa = familiaNumerosa;
        this.repetidor = repetidor;
        this.esperado = esperado;
    }
    
    @Test
    public void testMatricula(){
        assertEquals(esperado, m.calculaTasaMatricula(edad, familiaNumerosa, repetidor), 0.002f);
    }
}
