/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class MatriculaTestSinParametros {
    Matricula m = new Matricula();
    @Test
    public void testC1(){
        float resultadoReal = m.calculaTasaMatricula(19, false, true);
        float resultadoEsperado = 2000;
        assertEquals(resultadoEsperado, resultadoReal, 0.002f);
    }
    @Test
    public void testC2(){
        float resultadoReal = m.calculaTasaMatricula(68, false, true);
        float resultadoEsperado = 250;
        assertEquals(resultadoEsperado, resultadoReal, 0.002f);
    }
    @Test
    public void testC3(){
        float resultadoReal = m.calculaTasaMatricula(19, true, true);
        float resultadoEsperado = 250;
        assertEquals(resultadoEsperado, resultadoReal, 0.002f);
    }
    @Test
    public void testC4(){
        float resultadoReal = m.calculaTasaMatricula(19, false, false);
        float resultadoEsperado = 500;
        assertEquals(resultadoEsperado, resultadoReal, 0.002f);
    }
    @Test
    public void testC5(){
        float resultadoReal = m.calculaTasaMatricula(61, false, false);
        float resultadoEsperado = 400;
        assertEquals(resultadoEsperado, resultadoReal, 0.002f);
    }
}
